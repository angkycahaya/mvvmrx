package mvvm.buffgix.com.mvvmtools.presentation.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mvvm.buffgix.com.mvvmtools.R;
import mvvm.buffgix.com.mvvmtools.databinding.ItemUserBinding;
import mvvm.buffgix.com.mvvmtools.presentation.model.User;
import mvvm.buffgix.com.mvvmtools.presentation.view.viewHolder.UserViewHolder;

/**
 * Created by PEGIPEGI ROG on 3/20/2018.
 * Angky Cahaya Putra
 */

public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {

    private List<User> data;

    public UserAdapter() {
        data = new ArrayList<>();
    }

    public void setData(List<User> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemUserBinding itemUserBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_user, parent, false);
        return new UserViewHolder(itemUserBinding);
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {
        holder.bindUser(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
