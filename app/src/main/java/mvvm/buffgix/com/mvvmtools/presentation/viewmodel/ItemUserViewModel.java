package mvvm.buffgix.com.mvvmtools.presentation.viewmodel;

import android.content.Context;

import java.util.Observable;

import mvvm.buffgix.com.mvvmtools.presentation.model.User;

/**
 * Created by PEGIPEGI ROG on 3/20/2018.
 * Angky Cahaya Putra
 */

public class ItemUserViewModel extends Observable {

    private User user;

    public ItemUserViewModel(User user) {
        this.user = user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return user.getName();
    }

    public String getUsername() {
        return user.getUsername();
    }

    public String getEmail() {
        return user.getEmail();
    }

    public String getAddress() {
        return user.getAddress();
    }

    public String getPhone() {
        return user.getPhone();
    }

    public String getWebsite() {
        return user.getWebsite();
    }

    public String getCompany() {
        return user.getCompany();
    }

}
