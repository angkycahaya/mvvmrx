package mvvm.buffgix.com.mvvmtools.data.api.model;

import java.io.Serializable;

/**
 * Created by PEGIPEGI ROG on 3/18/2018.
 * Angky Cahaya Putra
 */

public class CompanyAPI implements Serializable {

    private String name;
    private String catchPhrase;
    private String bs;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public void setCatchPhrase(String catchPhrase) {
        this.catchPhrase = catchPhrase;
    }

    public String getBs() {
        return bs;
    }

    public void setBs(String bs) {
        this.bs = bs;
    }
}
