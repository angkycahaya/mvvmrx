package mvvm.buffgix.com.mvvmtools.domain.converter;

import java.util.List;

import mvvm.buffgix.com.mvvmtools.data.api.model.UserAPI;
import mvvm.buffgix.com.mvvmtools.data.local.model.UserRealm;
import mvvm.buffgix.com.mvvmtools.presentation.model.User;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public interface UserConverter {

    List<User> getConvertedUserFromAPI(List<UserAPI> userRespons);

    List<User> getConvertedUserFromLocal(List<UserRealm> userRealms);
}
