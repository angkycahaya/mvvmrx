package mvvm.buffgix.com.mvvmtools.data.api.model;

import java.io.Serializable;

/**
 * Created by PEGIPEGI ROG on 3/18/2018.
 * Angky Cahaya Putra
 */

public class UserAPI implements Serializable {

    private String id;
    private String name;
    private String username;
    private String email;
    private AddressAPI address;
    private String phone;
    private String website;
    private CompanyAPI company;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressAPI getAddress() {
        return address;
    }

    public void setAddress(AddressAPI address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public CompanyAPI getCompany() {
        return company;
    }

    public void setCompany(CompanyAPI company) {
        this.company = company;
    }
}
