package mvvm.buffgix.com.mvvmtools.domain.factory;

import android.content.Context;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 */

public interface UserFactory {

    void getUserListAPI(Context context);
}
