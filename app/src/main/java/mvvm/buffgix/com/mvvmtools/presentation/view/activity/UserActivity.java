package mvvm.buffgix.com.mvvmtools.presentation.view.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;

import java.util.Observable;
import java.util.Observer;

import mvvm.buffgix.com.mvvmtools.R;
import mvvm.buffgix.com.mvvmtools.databinding.ActivityUserBinding;
import mvvm.buffgix.com.mvvmtools.presentation.view.adapter.UserAdapter;
import mvvm.buffgix.com.mvvmtools.presentation.viewmodel.UserViewModel;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public class UserActivity extends BaseActivity implements Observer {

    private ActivityUserBinding activityUserBinding;
    private UserViewModel userViewModel;

    @Override
    protected void initDataBinding() {
        activityUserBinding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        userViewModel = new UserViewModel(this);
        activityUserBinding.setUserViewModel(userViewModel);
    }

    @Override
    protected void initView() {
        initObserver();
        initAdapter();
    }

    private void initObserver() {
        userViewModel.addObserver(this);
    }

    private void initAdapter() {
        UserAdapter userAdapter = new UserAdapter();
        activityUserBinding.recyclerViewUser.setAdapter(userAdapter);
        activityUserBinding.recyclerViewUser.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void update(Observable observable, Object object) {
        if (observable instanceof UserViewModel) {
            UserAdapter userAdapter = (UserAdapter) activityUserBinding.recyclerViewUser.getAdapter();
            UserViewModel userViewModel = (UserViewModel) observable;
            userAdapter.setData(userViewModel.getData());
        }
    }
}
