package mvvm.buffgix.com.mvvmtools.domain.factory;

import android.content.Context;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import mvvm.buffgix.com.mvvmtools.app.AppController;
import mvvm.buffgix.com.mvvmtools.data.api.model.UserAPI;
import mvvm.buffgix.com.mvvmtools.data.api.service.UserService;
import mvvm.buffgix.com.mvvmtools.domain.converter.UserConverter;
import mvvm.buffgix.com.mvvmtools.presentation.model.User;
import mvvm.buffgix.com.mvvmtools.util.config.Constant;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public class UserFactoryImpl implements UserFactory {

    private UserConverter userConverter;
    private Observer<List<User>> observerUser;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public UserFactoryImpl(UserConverter userConverter, Observer<List<User>> observerUser) {
        this.userConverter = userConverter;
        this.observerUser = observerUser;
    }

    private Consumer<List<UserAPI>> getConsumer() {
        return new Consumer<List<UserAPI>>() {

            @Override
            public void accept(List<UserAPI> userRespons) throws Exception {
                List<User> data = userConverter.getConvertedUserFromAPI(userRespons);
                observerUser.onNext(data);
                observerUser.onComplete();
            }
        };
    }

    private Consumer<Throwable> getConsumerThrowable() {
        return new Consumer<Throwable>() {

            @Override
            public void accept(Throwable throwable) throws Exception {
                observerUser.onError(throwable);
            }
        };
    }

    @Override
    public void getUserListAPI(Context context) {
        AppController appController = AppController.create(context);
        UserService userService = appController.getUserService();
        Disposable disposable = userService.getUsers(Constant.USER_URL)
                .subscribeOn(appController.subscribeScheduler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getConsumer(), getConsumerThrowable());
        compositeDisposable.add(disposable);
    }
}
