package mvvm.buffgix.com.mvvmtools.presentation.viewmodel;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import mvvm.buffgix.com.mvvmtools.R;
import mvvm.buffgix.com.mvvmtools.util.config.Constant;
import mvvm.buffgix.com.mvvmtools.util.helper.ListValidator;
import mvvm.buffgix.com.mvvmtools.domain.converter.UserConverter;
import mvvm.buffgix.com.mvvmtools.domain.converter.UserConverterImpl;
import mvvm.buffgix.com.mvvmtools.domain.factory.UserFactory;
import mvvm.buffgix.com.mvvmtools.domain.factory.UserFactoryImpl;
import mvvm.buffgix.com.mvvmtools.presentation.model.User;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public class UserViewModel extends Observable {

    private ObservableInt progressBarVisibility;
    private ObservableInt buttonReloadVisibility;
    private ObservableInt userRecyclerViewVisibility;
    private ObservableInt userLabelVisibility;
    private ObservableField<String> userLabelText;

    private UserFactory userFactory;
    private List<User> data;
    private Context context;

    public UserViewModel(@NonNull Context context) {
        this.context = context;
        init();
    }

    private void init() {
        UserConverter userConverter = new UserConverterImpl();
        userFactory = new UserFactoryImpl(userConverter, getObserver());
        setViewState(Constant.INIT_STATE);
    }

    public void onClickUserLoad(View view) {
        setViewState(Constant.LOADING_STATE);
        userFactory.getUserListAPI(context);
    }

    public List<User> getData() {
        return data;
    }

    private void setViewState(int state) {
        switch (state) {
            case Constant.INIT_STATE:
                progressBarVisibility = new ObservableInt(View.GONE);
                buttonReloadVisibility = new ObservableInt(View.VISIBLE);
                userRecyclerViewVisibility = new ObservableInt(View.GONE);
                userLabelVisibility = new ObservableInt(View.GONE);
                userLabelText = new ObservableField<>(context.getString(R.string.msg_click_to_load));
                break;
            case Constant.LOADING_STATE:
                progressBarVisibility.set(View.VISIBLE);
                buttonReloadVisibility.set(View.GONE);
                userRecyclerViewVisibility.set(View.GONE);
                userLabelVisibility.set(View.GONE);
                break;
            case Constant.SUCCESS_STATE:
                progressBarVisibility.set(View.GONE);
                buttonReloadVisibility.set(View.GONE);
                userRecyclerViewVisibility.set(View.VISIBLE);
                userLabelVisibility.set(View.GONE);
                break;
            case Constant.EMPTY_STATE:
                progressBarVisibility.set(View.GONE);
                buttonReloadVisibility.set(View.GONE);
                userRecyclerViewVisibility.set(View.GONE);
                userLabelVisibility.set(View.VISIBLE);
                userLabelText.set(context.getString(R.string.msg_empty_data));
                break;
            case Constant.ERROR_STATE:
                progressBarVisibility.set(View.GONE);
                buttonReloadVisibility.set(View.GONE);
                userRecyclerViewVisibility.set(View.GONE);
                userLabelVisibility.set(View.VISIBLE);
                userLabelText.set(context.getString(R.string.msg_error_data));
                break;
        }
    }

    private Observer<List<User>> getObserver() {
        return new Observer<List<User>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(List<User> users) {
                if (!ListValidator.isNullOrEmpty(users)) {
                    notifyObserver(users);
                    setViewState(Constant.SUCCESS_STATE);
                } else {
                    setViewState(Constant.EMPTY_STATE);
                }
            }

            @Override
            public void onError(Throwable e) {
                setViewState(Constant.ERROR_STATE);
            }

            @Override
            public void onComplete() {
            }
        };
    }

    private void notifyObserver(List<User> result) {
        data = new ArrayList<>();
        data.addAll(result);
        setChanged();
        notifyObservers();
    }

    /**
     * These section below represent getter for view binding
    * */
    public ObservableInt getProgressBarVisibility() {
        return progressBarVisibility;
    }

    public ObservableInt getButtonReloadVisibility() {
        return buttonReloadVisibility;
    }

    public ObservableInt getUserRecyclerViewVisibility() {
        return userRecyclerViewVisibility;
    }

    public ObservableInt getUserLabelVisibility() {
        return userLabelVisibility;
    }

    public ObservableField<String> getUserLabelText() {
        return userLabelText;
    }
}
