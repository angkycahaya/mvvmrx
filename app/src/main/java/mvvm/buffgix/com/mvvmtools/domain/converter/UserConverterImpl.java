package mvvm.buffgix.com.mvvmtools.domain.converter;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import mvvm.buffgix.com.mvvmtools.data.api.model.UserAPI;
import mvvm.buffgix.com.mvvmtools.data.local.model.UserRealm;
import mvvm.buffgix.com.mvvmtools.presentation.model.User;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public class UserConverterImpl implements UserConverter {

    private String getValidAddressFromAPI(UserAPI userAPI) {
        if (userAPI != null
                && userAPI.getAddress() != null
                && !TextUtils.isEmpty(userAPI.getAddress().getSuite())
                && !TextUtils.isEmpty(userAPI.getAddress().getStreet())
                && !TextUtils.isEmpty(userAPI.getAddress().getCity())
                && !TextUtils.isEmpty(userAPI.getAddress().getZipcode())) {
            return userAPI.getAddress().getSuite() +
                    ", " +
                    userAPI.getAddress().getStreet() +
                    ", " +
                    userAPI.getAddress().getCity() +
                    " " +
                    userAPI.getAddress().getZipcode();
        } else {
            return "";
        }
    }

    private String getValidCompanyFromAPI(UserAPI userAPI) {
        if (userAPI != null
                && userAPI.getCompany() != null
                && !TextUtils.isEmpty(userAPI.getCompany().getName())
                && !TextUtils.isEmpty(userAPI.getCompany().getCatchPhrase())
                && !TextUtils.isEmpty(userAPI.getCompany().getBs())) {
            return userAPI.getCompany().getName() +
                    ", " +
                    userAPI.getCompany().getCatchPhrase() +
                    ", " +
                    userAPI.getCompany().getBs();
        } else {
            return "";
        }
    }

    private User convertUserFromAPI(UserAPI userAPI) {
        User user = new User();
        user.setId(userAPI.getId());
        user.setName(userAPI.getName());
        user.setUsername(userAPI.getUsername());
        user.setEmail(userAPI.getEmail());
        String fullAddress = getValidAddressFromAPI(userAPI);
        user.setAddress(fullAddress);
        user.setPhone(userAPI.getPhone());
        user.setWebsite(userAPI.getWebsite());
        String fullCompany = getValidCompanyFromAPI(userAPI);
        user.setCompany(fullCompany);
        return user;
    }

    @Override
    public List<User> getConvertedUserFromAPI(List<UserAPI> userRespons) {
        List<User> user = new ArrayList<>();
        for (UserAPI userAPI : userRespons) {
            user.add(convertUserFromAPI(userAPI));
        }
        return user;
    }

    private User convertUserFromLocal(UserRealm userRealm) {
        User user = new User();
        user.setId(userRealm.getId());
        user.setName(userRealm.getName());
        user.setUsername(userRealm.getUsername());
        user.setEmail(userRealm.getEmail());
        user.setAddress(userRealm.getAddress());
        user.setPhone(userRealm.getPhone());
        user.setWebsite(userRealm.getWebsite());
        user.setCompany(userRealm.getCompany());
        return user;
    }

    @Override
    public List<User> getConvertedUserFromLocal(List<UserRealm> userRealms) {
        List<User> user = new ArrayList<>();
        for (UserRealm userRealm : userRealms) {
            user.add(convertUserFromLocal(userRealm));
        }
        return user;
    }

}
