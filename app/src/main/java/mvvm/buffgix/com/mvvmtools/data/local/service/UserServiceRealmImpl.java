package mvvm.buffgix.com.mvvmtools.data.local.service;

import java.util.List;

import io.realm.Realm;
import mvvm.buffgix.com.mvvmtools.data.local.model.UserRealm;

/**
 * Created by PEGIPEGI ROG on 3/22/2018.
 * Angky Cahaya Putra
 */

public class UserServiceRealmImpl implements UserServiceRealm {

    private final String ID_COLUMN = "id";
    private Realm realm;

    public UserServiceRealmImpl() {
        realm = RealmHelper.getInstance().getRealm();
    }

    @Override
    public void clearAll() {
        realm.beginTransaction();
        realm.delete(UserRealm.class);
        realm.commitTransaction();
    }

    @Override
    public boolean hasItem() {
        return !realm.where(UserRealm.class).findAll().isEmpty();
    }

    @Override
    public long getItemCount() {
        return realm.where(UserRealm.class).count();
    }

    @Override
    public void insertOrUpdate(UserRealm userRealm) {
        realm.beginTransaction();
        realm.insertOrUpdate(userRealm);
        realm.commitTransaction();
    }

    @Override
    public void insertOrUpdate(List<UserRealm> userRealms) {
        realm.beginTransaction();
        realm.insertOrUpdate(userRealms);
        realm.commitTransaction();
    }

    @Override
    public void delete(String id) {
        getDataById(id).deleteFromRealm();
    }

    @Override
    public List<UserRealm> getData() {
        return realm.copyFromRealm(realm.where(UserRealm.class).findAll());
    }

    @Override
    public UserRealm getDataById(String id) {
        return realm.where(UserRealm.class).equalTo(ID_COLUMN, id).findFirst();
    }

}
