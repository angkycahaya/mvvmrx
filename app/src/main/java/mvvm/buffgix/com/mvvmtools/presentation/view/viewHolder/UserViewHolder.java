package mvvm.buffgix.com.mvvmtools.presentation.view.viewHolder;

import android.support.v7.widget.RecyclerView;

import mvvm.buffgix.com.mvvmtools.databinding.ItemUserBinding;
import mvvm.buffgix.com.mvvmtools.presentation.model.User;
import mvvm.buffgix.com.mvvmtools.presentation.viewmodel.ItemUserViewModel;

/**
 * Created by PEGIPEGI ROG on 3/20/2018.
 * Angky Cahaya Putra
 */

public class UserViewHolder extends RecyclerView.ViewHolder {

    private ItemUserBinding itemUserBinding;

    public UserViewHolder(ItemUserBinding itemUserBinding) {
        super(itemUserBinding.linearLayoutUserItem);
        this.itemUserBinding = itemUserBinding;
    }

    public void bindUser(User user) {
        if (itemUserBinding.getItemUserViewModel() == null) {
            itemUserBinding.setItemUserViewModel(new ItemUserViewModel(user));
        } else {
            itemUserBinding.getItemUserViewModel().setUser(user);
        }
    }
}
