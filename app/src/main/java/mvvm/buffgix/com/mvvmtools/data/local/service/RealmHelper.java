package mvvm.buffgix.com.mvvmtools.data.local.service;

import io.realm.Realm;

/**
 * Created by PEGIPEGI ROG on 3/22/2018.
 * Angky Cahaya Putra
 */

public class RealmHelper {

    private static RealmHelper realmHelper;
    private final Realm realm;

    private RealmHelper() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmHelper getInstance() {
        if (realmHelper == null) {
            synchronized (RealmHelper.class) {
                if (realmHelper == null) {
                    realmHelper = new RealmHelper();
                }
            }
        }
        return realmHelper;
    }

    public Realm getRealm() {
        return realm;
    }
}
