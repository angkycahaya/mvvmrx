package mvvm.buffgix.com.mvvmtools.app;

import android.app.Application;
import android.content.Context;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import mvvm.buffgix.com.mvvmtools.data.api.service.APIFactory;
import mvvm.buffgix.com.mvvmtools.data.api.service.UserService;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public class AppController extends Application {

    private UserService userService;
    private Scheduler scheduler;

    @Override
    public void onCreate() {
        super.onCreate();
        initRealm();
    }

    private static AppController get(Context context) {
        return (AppController) context.getApplicationContext();
    }

    public static AppController create(Context context) {
        return AppController.get(context);
    }

    public UserService getUserService() {
        if (userService == null) {
            synchronized (AppController.class) {
                if (userService == null) {
                    userService = APIFactory.create();
                }
            }
        }
        return userService;
    }

    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            synchronized (AppController.class) {
                if (scheduler == null) {
                    scheduler = Schedulers.io();
                }
            }
        }
        return scheduler;
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("mvvm_rx.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
