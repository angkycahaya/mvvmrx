package mvvm.buffgix.com.mvvmtools.data.local.service;

import java.util.List;

import mvvm.buffgix.com.mvvmtools.data.local.model.UserRealm;

/**
 * Created by PEGIPEGI ROG on 3/22/2018.
 * Angky Cahaya Putra
 */

public interface UserServiceRealm {

    void insertOrUpdate(UserRealm userRealm);

    void insertOrUpdate(List<UserRealm> userRealm);

    boolean hasItem();

    void clearAll();

    long getItemCount();

    void delete(String id);

    public List<UserRealm> getData();

    public UserRealm getDataById(String id);
}
