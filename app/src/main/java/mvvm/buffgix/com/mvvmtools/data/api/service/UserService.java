package mvvm.buffgix.com.mvvmtools.data.api.service;

import java.util.List;

import io.reactivex.Observable;
import mvvm.buffgix.com.mvvmtools.data.api.model.UserAPI;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public interface UserService {

    @GET
    Observable<List<UserAPI>> getUsers(@Url String url);

}
