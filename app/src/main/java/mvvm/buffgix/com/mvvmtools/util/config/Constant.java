package mvvm.buffgix.com.mvvmtools.util.config;

/**
 * Created by PEGIPEGI ROG on 3/19/2018.
 * Angky Cahaya Putra
 */

public class Constant {

    public static final String BASE_URL = "https://jsonplaceholder.typicode.com/";
    public static final String USER_URL = BASE_URL + "users/";

    public static final int INIT_STATE = 0;
    public static final int LOADING_STATE = 1;
    public static final int SUCCESS_STATE = 2;
    public static final int EMPTY_STATE = 3;
    public static final int ERROR_STATE = 4;

}
