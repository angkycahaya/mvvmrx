package mvvm.buffgix.com.mvvmtools.util.helper;

import java.util.List;

/**
 * Created by PEGIPEGI ROG on 3/20/2018.
 * Angky Cahaya Putra
 */

public class ListValidator {

    public static boolean isNullOrEmpty(List data) {
        return data == null || data.size() == 0;
    }

}
